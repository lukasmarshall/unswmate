#!/usr/bin/perl -w
#why wont it work on the webserver?!
# written by andrewt@cse.unsw.edu.au September 2012
# as a starting point for COMP2041/9041 assignment 2
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/unswmate/

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use List::Util qw/min max/;
warningsToBrowser(1);

# print start of HTML ASAP to assist debugging if there is an error in the script
print page_header();

# some globals used through the script
$debug = 1;
$users_dir = "./users";

my $username = param('username');
if (! $username) {
    # for the purposes of level 0 testing if no username is supplied
    # we select a random username
    #
    # in level 1 if no username is supplied 
    # your website should allow a username to found by searching
    # or a user tologin by supplying their username and password
    
    my @users = glob("$users_dir/*");
    $username = $users[rand @users];
    $username =~ s/.*\///;
}

print browse_screen();
print page_trailer();
exit 0; 


sub browse_screen {
    my $details_filename = "$users_dir/$username/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $!";
    $details = join '', <$p>;
    close $p;
    
    return p,
        start_form, "\n",
        submit('Random UNSW Mate Page'), "\n",
        pre($details),"\n",
        end_form, "\n",
        p, "\n";
}

#
# HTML placed at the top of every screen
#
sub page_header {
    return header,
        start_html("-title"=>"UNSW Mate", -bgcolor=>"#FEDCBA"),
        center(h2(i("UNSW Mate")));
}

#
# HTML placed at bottom of every screen
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer {
    my $html = "";
    $html .= join("", map("<!-- $_=".param($_)." -->\n", param())) if $debug;
    $html .= end_html;
    return $html;
}