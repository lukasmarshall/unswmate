#!/usr/bin/perl 

# written by andrewt@cse.unsw.edu.au September 2012
# as a starting point for COMP2041/9041 assignment 2
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/unswmate/# 



#Now Implemented:
# GALLERY (LEVEL 1)
# SEARCH FOR USERNAMES (LEVEL 1)
# LOGIN & LOGOUT (LEVEL 1)
# CREATING UNSW-MATE PAGES (LEVEL 2)
# E-MAIL CONFIRMATION (LEVEL 2)
# UPLOADING "ABOUT ME" (LEVEL 2)
# UPLOADING IMAGES (LEVEL 2)
# MATE REQUESTS (LEVEL 2)


#Still To Implement:

# MATE SUGGESTIONS (LEVEL 3)
# NEWS ITEMS (LEVEL 4)
# COMMENTS (LEVEL 4)
# PRIVACY(LEVEL 4)
# OTHER FEATURES (LEVEL 4)



use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use List::Util qw/min max/; 
# use Email::MIME;
# use Email::Sender::Simple;
use Mail::Mailer;

sub getListFromHeading($$);
sub display_profile;
sub display_searchPage;
sub handleSearchResults;
sub display_notFoundPage;
sub browse_screen;
sub image_display;
sub print_headerBar;
sub page_header;
sub page_trailer;
sub mates_icons;
sub user_name_heading;
sub print_searchBar;
sub find_user($);
sub getAllUserNames;
sub user_details;
sub searchNotFoundImage_display();

sub print_galleryButton;
sub print_user_aboutme;

sub print_galleryPics;

sub getAllGalleryFileNames;

sub signup_link;
sub display_signup;
sub signupForm_display;
sub display_signup_confirmation;
sub create_user;
sub processProfileImageUpload;
sub changeUserInfo($$$);
sub getAboutMe;
sub print_login_forms;
sub print_login_button;
sub checkLoginDetails;
sub print_searchButton;
sub sendConfirmationEmail;
sub display_userNotFoundPage;
sub print_addFriendButton;
sub sendFriendRequestEmail;
sub addFriend($$);



warningsToBrowser(1);

# print start of HTML ASAP to assist debugging if there is an error in the script
print page_header();

# some globals used through the script
$debug = 1;
$users_dir = "./users";
$images_dir = "./images";
$cgi_url = "http://cgi.cse.unsw.edu.au/~lama480/cgi-bin/unswmate.cgi";

$loggedInUser = param('loggedInUser');

$username = param('username'); 
$searchText = param('search_text');

$uploadProfilePic = param('uploaded_file');

$mode = param('mode');
unless($mode){
        $mode = 'default';
}


#if we are changing a user's info, change it before displaying stuff.
if(param('change_user_info')){
    changeUserInfo(param('change_user_info'), param('editedText'), $username);
}




if($mode eq 'login_user'){
    if(checkLoginDetails()){
        my @confirmed = getListFromHeading("confirmed",param('login_username'));
        if($confirmed[0] eq "true"){
            
            display_profile();

        }else{
            $mode = 'newUser';
            display_searchPage();
        }
        
    }else{
        $mode = 'user_not_found';
        display_searchPage();
    }
}elsif($mode eq 'newUser'){
    create_user();
    sendConfirmationEmail();
    display_signup_confirmation();
}elsif($mode eq 'uploadingFile' && $username){
    processProfileImageUpload();
    display_profile();
}elsif($mode eq 'confirm_registration'){
    confirmRegistraionFromEmail();
}elsif($mode eq 'send_friendRequest'){
    
    sendFriendRequestEmail(); #send the confirmation email.
    $mode = 'request_sent';  #change mode to 'request sent' so that when the profile page is reloaded the button changes.
    display_profile(); #display the profile.
}elsif($mode eq'confirm_friend_request'){
    addFriend(param('person_accepting'), param('person_requesting'));
    addFriend(param('person_requesting'), param('person_accepting'));
    $username = param('person_accepting');
    display_profile();
}elsif($searchText){         #if no user is selected and there is a search, handle the search
    handleSearchResults();
}elsif($username){                                       #if a user is selected, go there.
    display_profile();
}else{
    display_searchPage();
}

exit 0; 

sub display_profile{
    
    print "<div id=\"body\">";
        print "<div id=\"header\">";
            print_headerBar(); #facebook image
        print "</div>";


        print "<div id=\"main\">\n";
            print "<div id=\"content-1\">\n";
                print "<center>";
                print user_name_heading(); #user name heading ie. 'Michael Phelps'
                image_display(); #Display profile Pic
                unless($username eq $loggedInUser){
                    if($mode eq 'request_sent'){
                        my $image_filename = "$images_dir/requestSent.png";
                        print "<img src=\"".$image_filename."?size=100\">";
                    }else{
                        print_addFriendButton();
                    }
                }
                print_galleryButton();
                print_profileButton();
                
                print_searchBar();
                print "</center>";
                print mates_icons();    #displays all icons of mates.
            print "</div>\n";
       
            print "<div id=\"content-2\">\n";
                print "<div id=\"content-2-1\">\n";
                    if($mode=~'gallery'){
                        print_galleryPics(); #display user gallery
                    }else{
                        print_user_aboutme();
                    }
                print "</div>\n";
                
                print "<div id=\"content-2-2\">\n";
                    print user_details(); #display user deets.
                   
                print "</div>\n";
            print "</div>\n";
        print "</div>\n";

        print "<div id=\"footer\"></div>\n";
    print "</div>\n";

    print page_trailer(); #end of page.
}


sub display_signup_confirmation{
    
    
    print "<br><br><br><br><br><br><br>";
    print "<center>";
    print_headerBar(); #Heading Image
    print "<br><br>";
    my $image_filename = "$images_dir/welcome.png";
    my $instructions_filename = "$images_dir/checkEmail.png";
    print "<img src=\"".$image_filename."?size=100\">";
    print "<img src=\"".$instructions_filename."?size=100\">";
    print "<br><br>";
    print "</center>";
    

    print page_trailer(); #end of page.
}



sub display_searchPage{
    print "<br><br><br><br><br><br><br>";
    print "<center>";
    print_headerBar(); #Heading Image
    print "<br><br>";
    
    

    if($mode eq 'display_searchBar'){
        print_searchBar(); #show search bar.
    }elsif($mode eq 'login_frontpage'){
        print_login_forms();
    }elsif($mode eq 'signUp'){
        print signupForm_display();
    }elsif($mode eq 'newUser'){
        my $image_filename = "$images_dir/stillMustConfirm.png";
        print "<img src=\"".$image_filename."?size=100\">";
    }elsif($mode eq 'new_user_confirmed'){
        my $image_filename = "$images_dir/thanksForRegistering.png";
        print "<img src=\"".$image_filename."?size=100\">";
        print_login_button();
    }elsif(mode eq 'user_not_found'){
        my $image_filename = "$images_dir/userNotFound.png";
        print "<img src=\"".$image_filename."?size=100\">";
        print_login_button();
    }else{
        print_searchButton();
        print signup_link();
        print_login_button();
    }

    print "<br><br>";
    print "</center>";
    

    print page_trailer(); #end of page.
}

sub handleSearchResults{
    find_user($searchText);
    
    if($username){
         display_profile();
    }else{
        display_notFoundPage();
    }
}

sub display_notFoundPage{
    print "<br><br><br><br><br><br><br>";
    print "<center>";
        print_headerBar(); #Heading Image
    print searchNotFoundImage_display(); #'not found' image
    print "<br><br>";
    print_searchBar(); #show search bar.
    print "</center>";
    print page_trailer(); #end of page.
}



sub image_display{
    my $image_filename = "$users_dir/$username/profile.jpg";
    

    #if the profile picture exists, display the image.
    if(-e $image_filename){
        print "\n";
        print "<img src=\"".$image_filename."? width=\"137\" height=\"137\">";
        print "\n";
    }else{ #Otherwise display an upload form.
        print start_multipart_form();
        
        print filefield(-name=>'uploaded_file',
                     -default=>'starting value',
                    -size=>50,
                    -maxlength=>80);
        print image_button(-name=>'signupLink_button',
                    -src=>"$images_dir/uploadProfilePic.png",
                    -align=>'MIDDLE');
        print hidden(-name=>'username',-value=>$username,-override=>1);
        print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
        print hidden(-name=>'mode',-value=>'uploadingFile',-override=>1);
        print "\n";
        print end_form;
        print "\n";
        print "\n";
    }    
    
}



# HTML placed at the top of every screen
sub page_header{

    return header,
        start_html("-title"=>"UNSW Mate", -bgcolor=>"#FFFFFF",-style=>{'src'=>'style.css'},);
}

#
# HTML placed at bottom of every screen
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer{
    my $html = "";
    $html .= join("", map("<!-- $_=".param($_)." -->\n", param())) if $debug;
    $html .= end_html;
    return $html;
}

sub mates_icons{
    my @matesList = getListFromHeading("mates", $username);
    my @imageTags;
    push @imageTags, "<h3>Friends<\h3><br>";
    for $user (@matesList){
        #get the directory of the user's profile pic.
        my $image_filename = $users_dir."/".$user."/profile.jpg";

        push @imageTags,  "<img src=\"".$image_filename."?size=100\" width=\"40\" height=\"40\">\n";
        
        #Get the user's name from their data file.
        my @name = getListFromHeading("name",$user);
        push @imageTags, "<a href=\"".$cgi_url."?username=".$user."\">".$name[0]."</a>";
        push @imageTags, "<br>\n";
    }
    return @imageTags;
}


sub user_name_heading{
    # die "Couldn't print heading - "."$username";#
    my @names = getListFromHeading("name",$username);
    my $name = $names[0];
    my $tag = "<h1>".$name."-mon</h1>";
    return $tag;
}

sub print_searchBar{
    print start_form(-name => 'search_form', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                );
    print textfield(
        -name      => 'search_text',
        -value     => '',
        -size      => 20,
        -maxlength => 30,
    );
    print image_button(-name=>'search_button',
                        -src=>"$images_dir/searchLogo.png",
                        -align=>'MIDDLE');
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print end_form();
}

sub find_user($){
    my @names = getAllUserNames();
    my $simplifiedSearch = $_[0];
    $simplifiedSearch =~ s/[\_|\s|\t|\.]//; 
    $simplifiedSearch = lc $simplifiedSearch;
    #die "$username $simplifiedSearch $simplifiedName";
    my $found = 0;
    for my $name (@names){
        my $simplifiedName = $name;

        $simplifiedName =~ s/[\_|\s|\t|\.]//;            #remove underscore
        $simplifiedName = lc $simplifiedName;                  #lower case.
        
        if ($simplifiedSearch =~ $simplifiedName && $simplifiedName =~ /[A-Z|a-z]/ && simplifiedSearch =~ /[A-Z|a-z]/){
            $username = $name;
            $found = 1;
            #die "match found = $found name = $name simName = $simplifiedName simSearch = $simplifiedSearch";
        }
            
    }
    if($found == 0){
        $username = ""; 
    }
    #die "found = $found  $simplifiedSearch";
}
sub getAllUserNames{
    opendir DIR, $users_dir or die "Couldn't open user directory!";
    my @files = readdir(DIR);
    closedir DIR;
    
    return @files;
}

sub searchNotFoundImage_display{

    my $image_filename = "$images_dir/notFound.png";
    my $imageTag = "<img src=\"".$image_filename."?size=100\">";
    return "\n",$imageTag,"\n";

}



sub user_details{
    my @gender = getListFromHeading("gender",$username);
    my @degree = getListFromHeading("degree",$username);
    my @courseList = getListFromHeading("courses",$username);
    my $onLoggedInUsersProfile = "";
    if($username eq $loggedInUser){
        
        $onLoggedInUsersProfile = "true";
    }
    #Gender
    print "<br><left>Gender: $gender[0]</left> ";
    #if the edit mode is set to gender, display a gender edit form.
    if(param('edit')){
        if(param('edit') =~ 'gender'){
            #display the text box form
            print start_form(-name => 'genderEdit', 
                                    -method  => 'GET', 
                                    -enctype => &CGI::URL_ENCODED,
                                    
                            );
            print textfield('editedText',$gender[0],30,80);
            print image_button(-name=>'edit_button',
                        -src=>"$images_dir/submit.png",
                        -align=>'LEFT');
            print hidden(-name=>'change_user_info', -value=>'gender',-override=>1);
            print hidden(-name=>'username', -value=>$username,-override=>1);
            print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
            print "\n";
            print end_form;
        }
    }else{ #otherwise display stored gender.
        print "<center>\n";
        if($onLoggedInUsersProfile){
            print_editButton('gender');
        }
        print "\n</center>\n"
    }
    #Degrees
    print "<br><left>Type: $degree[0]</left>";
    if(param('edit')){
        if(param('edit') =~ 'degree'){
            
            #display the text box form
            print start_form(-name => 'degreeEdit', 
                                    -method  => 'GET', 
                                    -enctype => &CGI::URL_ENCODED,
                                    
                            );
            print textfield('editedText',$degree[0],30,80);
            print hidden(-name=>'change_user_info', -value=>'degree',-override=>1);
            print hidden(-name=>'username',-value=>$username,-override=>1);
            print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
            print image_button(-name=>'edit_button',
                        -src=>"$images_dir/submit.png",
                        -align=>'LEFT');
            print "\n";
            print end_form;
        }
    }else{
        print "<center>\n";
        if($onLoggedInUsersProfile){
            print_editButton('degree');
        }
        print "</center>\n";
    }

    #Abilities

    print "<br><br>Abilities:<br>";
    for my $course (@courseList){
        print  " $course <br>";
    }

}

sub print_galleryButton{
    print "<center>";
    print start_form(-name => 'gallery_form', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                                );
    print image_button(-name=>'gallery_button',
                -src=>"$images_dir/galleryButton.png",
                -align=>'MIDDLE');
    print hidden(-name=>'username',-value=>$username,-override=>1);
    print hidden(-name=>'mode',-value=>'gallery',-override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print "</center>";
    print end_form;
}

sub print_profileButton{
    print "<center>";
    print start_form(-name => 'profile_form', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                                );
    print image_button(-name=>'profile_button',
                -src=>"$images_dir/profileButton.png",
                -align=>'MIDDLE');
    print hidden(-name=>'username',-value=>$username,-override=>1);
    print hidden(-name=>'mode',-value=>'profile',-override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print "</center>";
    print end_form;
}


sub print_user_aboutme{
    print "\n<center>\n";
    print  "\n <input type=\"image\" src=".$images_dir."/aboutMeHeading.png />\n";
    print "<br>";
    #if we are in aboutme edit mode
    if(param('edit')){
        if(param('edit') =~ 'aboutme'){

            #display the text box form
            print start_form(-name => 'aboutMeEditButton', 
                                    -method  => 'GET', 
                                    -enctype => &CGI::URL_ENCODED,
                                    -value     => 'Gallery',
                            );
            print textarea(-name=>'editedText',
                  -default=>getAboutMe(),
                  -rows=>10,
                  -columns=>50);
            print image_button(-name=>'edit_button',
                        -src=>"$images_dir/submit.png",
                        -align=>'LEFT');
            print hidden(-name=>'change_user_info', -value=>'aboutme',-override=>1);
            print hidden(-name=>'username', -value=>$username,-override=>1);
            print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
            print "\n";
            print end_form;
        }
    }else{
        #otherwise display 'about me' text.
        print getAboutMe();
        print "<br>";
        if($username eq $loggedInUser){
        #then display the 'edit' button.
        print start_form(-name => 'aboutMeEditButton', 
                                    -method  => 'GET', 
                                    -enctype => &CGI::URL_ENCODED,
                                    
                                    );
        print hidden(-name=>'edit',-value=>'aboutme',-override=>);
        print hidden(-name=>'username', -value=>$username,-override=>);
        print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
        print image_button(-name=>'edit_button',
                    -src=>"$images_dir/edit.png",
                    -align=>'LEFT');
        print "\n";
        print end_form;
        }
    }
    print "\n";
    print "\n";
    print "\n</center>\n";
}

#gets a list of the given traitis of a user. For example, pass "mates" and a username, gives a list of a user's mates.
sub getListFromHeading($$){
    my $heading = $_[0];
    my $user = $_[1];
    # die "lala."."$user";
    my $details_filename = "./users/$user/details.txt";
    my @usernames;

    open FILE, $details_filename;
    my @lines = <FILE>;
    $inMatesList = 0;
    for $line (@lines){
        #check if line contains a colon - if so, a heading line.
        if($line =~ /.*:.*/){
            $inMatesList = 0;
        }

        #if we are in the middle of the mates list, add the line to the list of usernames.
        if($inMatesList == 1){ 
            $line =~ s/^\s+//;
            chomp $line;
            push @usernames, $line;
        }
        
        if($line =~ /^$heading:.*/){
            $inMatesList = 1;
        }

    }
    return @usernames;
}




sub print_galleryPics{
    
    print "<center><h3>Gallery<\h3><br>";
    my $imagesPerLine = 2;
    my $imagesOnLineCounter = 0;
    my @galleryFileNames = getAllGalleryFileNames();
    for my $galleryFileName (@galleryFileNames){
        

        my $image_filename = "$users_dir/$username/$galleryFileName";

        print  "\n <input type=\"image\" src=".$image_filename." width=\"100\" height=\"100\" />\n";
        
        $imagesPerLine++;
        if($imagesOnLineCounter == $imagesPerLine){
            print "<br>\n";
            $imagesOnLineCounter = 0;
        }
        
    }
    print "</center><br>";
    return @imageTags;

}

sub getAllGalleryFileNames{
    opendir DIR, $users_dir."/".$username or die "Couldn't open user directory!";
    my @files = readdir(DIR);

    closedir DIR;
    
    my @galleryFiles;
    for my $filename (@files){
        if($filename =~ /.*gallery.*/){
            push @galleryFiles, $filename;
        }
    }

    return @galleryFiles;
}



sub signup_link{
    my @output;
    push @output, start_form(-name => 'signupLink_form', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                                );
    push @output, image_button(-name=>'signupLink_button',
                -src=>"$images_dir/signUp.png",
                -align=>'MIDDLE');
    push @output, hidden(-name=>'mode',-value=>'signUp',-override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    push @output, "\n";
    push @output, end_form;
    push @output, "\n";
    push @output, "\n";
    return @output;
}

sub signupForm_display{
    my @output;
    print start_form(-name => 'signup_newUser_form', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                                );
    print "\n<br>\n";
    print "First:<br>";
    print textfield(-name=>'new_firstname',
                             -value=>'First',
                              -size=>50,
                             -maxlength=>80);
    print "\n<br>\n";
    print "Last:<br>";
    print textfield(-name=>'new_lastname',
                             -value=>'Last',
                              -size=>50,
                             -maxlength=>80);
    print "\n<br>\n";
    print "Username:<br>";
    print textfield(-name=>'new_username',
                             -value=>'First_Last',
                              -size=>50,
                             -maxlength=>80);
    print "\n<br>\n";
    print "Email:<br>";
    print textfield(-name=>'new_email',
                             -value=>'pikachu@pokemail.com',
                              -size=>50,
                             -maxlength=>80);
    print "\n<br>\n";
    print "Password:<br>";

    print password_field(-name=>'new_password',
                                -value=>'',
                                -size=>50,
                                -maxlength=>80);
    print "\n<br>\n";
    print hidden(-name=>'mode',-value=>'newUser',-override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print image_button(-name=>'register_button',
                -src=>"$images_dir/register.png",
                -align=>'MIDDLE');

    
    print end_form;
    
    
}

sub create_user{
    
    #variables to store new user data.
    my $newUsername = param('new_username');
    my $newPassword = param('new_password');
    my $newEmail = param('new_email');
    my $newFirstName = param('new_firstname');
    my $newLastname = param('new_lastname');

    #create the folder in the users directory.
    mkdir "$users_dir/$newUsername";
    chmod 0777, "$users_dir/$newUsername" or die $!;

    #directory of the new user's details.
    my $newFileString = "$users_dir/$newUsername/details.txt";

    #Print the new user's details to their 'details.txt' file.
    open (MYFILE, '>',$newFileString) or die $!;
    print MYFILE "name:\n";
    print MYFILE "     $newFirstName $newLastname\n";
    print MYFILE "username:\n";
    print MYFILE "     $newUsername\n";
    print MYFILE "password:\n";
    print MYFILE "     $newPassword\n";
    print MYFILE "email:\n";
    print MYFILE "     $newEmail\n";
    print MYFILE "gender:\n";
    print MYFILE "degree:\n";
    print MYFILE "courses:\n";
    

    
    #close the file. New user created! Woo!
    close (MYFILE) or die "couldn't close file"; 

}
sub processProfileImageUpload{

    #Ensure that we have the correct permissions to write the profile image to the user's directory.
    chmod 0777, "$users_dir/$username" or die $!;
    #new profile pic's directory.
    my $output_file = "$users_dir/$username/profile.jpg";

    #create a byte read limit and buffer.
    my ($bytesread, $buffer);
    my $numbytes = 1024;

    #open the output file, ie - the new 'profile.jpg' picture and copy it's contents from the user's workstation.
    open (OUTFILE, ">", "$output_file") 
        or die "Couldn't open $output_file for writing: $!";
    while ($bytesread = read($uploadProfilePic, $buffer, $numbytes)) {
        print OUTFILE $buffer;
    }
    #close the file. Profile pic uploaded! :D
    close OUTFILE;
}

sub changeUserInfo($$$){
    my $infoField = $_[0];
    my $newData = $_[1];
    my $userToModify = $_[2];
    my $found = "";
    chmod 0777, "$users_dir/$userToModify" or die $!;
    # if they were editing 'about me,' change the 'about me' file.
    if($infoField =~ 'aboutme'){
        my $aboutMe_filename = "$users_dir/$userToModify/aboutMe.txt";
        open (OUTFILE, ">", "$aboutMe_filename") 
        or die "Couldn't open $aboutMe_filename for writing: $!";
        print OUTFILE $newData;
        close OUTFILE;
    }else{#otherwise edit the details file.
        #directory of the details file.
        my $details_filename = "$users_dir/$userToModify/details.txt";

        #get the original details file.
        open (INFILE, "<", "$details_filename") or die "Couldn't open $details_filename for reading: $!";
        my @originalInfo = <INFILE>;
        close INFILE;

        my $inSelectedField = "";
        my @updatedInfo;
        for my $line (@originalInfo){
            
            if($inSelectedField =~ "true"){
                if($line =~ /^([a-z]|[A-Z])/){
                    $inSelectedField = "";
                    push @updatedInfo, $line;
                }
            }else{
                push @updatedInfo, $line;
                if($line =~/^$infoField.*/){
                    $found = "true";
                    $inSelectedField = "true";
                    push @updatedInfo, "     ";
                    push @updatedInfo, $newData;
                    push@updatedInfo, "\n";
                }
            }

        }
        if(!$found){
            push @updatedInfo, "\n";
            push @updatedInfo, $infoField;
            push @updatedInfo, "\n";
            push @updatedInfo, "     ";
            push @updatedInfo, $newData;
            push @updatedInfo, "\n";
        }

        #write the updates to the file.
        open (OUTFILE, ">", "$details_filename") or die "Couldn't open $details_filename for writing: $!";
        print OUTFILE @updatedInfo;
        close OUTFILE;
    }
}

sub getAboutMe{
    my $aboutMe_filename = "./users/$username/aboutMe.txt";

    open (OUTFILE, "<", "$aboutMe_filename");
    my @lines = <OUTFILE>;
    return @lines;
}

sub print_editButton($){
    my $formToEdit = $_[0];
    #then display the 'edit' button.
    print start_form(-name => '$formToEdit_EditButton', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );
    print hidden(-name=>'edit',-value=>$formToEdit,-override=>);
    print hidden(-name=>'username', -value=>$username,-override=>);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print image_button(-name=>'edit_button',
                    -src=>"$images_dir/edit.png",
                    -align=>'LEFT');
    print "\n";
    print end_form;
}


sub print_login_forms{
    print start_form(-name => 'loginForm', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );
    print hidden(-name=>'mode', -value=>"login_user", -override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print "Username:";

    print  textfield(
        -name      => 'login_username',
        -value     => '',
        -size      => 20,
        -maxlength => 30,
    );

    print "<br>Password:";

    print password_field(-name=>'login_password',
                                -value=>'',
                                -size=>20,
                                -maxlength=>30);
    
    print "<br>";

    print image_button(-name=>'login_button',
                -src=>"$images_dir/login_big.png",
                -align=>'MIDDLE');
    
    print end_form;
}

sub print_login_button{
    print start_form(-name => 'loginButton', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );

    print image_button(-name=>'login_button',
                -src=>"$images_dir/login_big.png",
                -align=>'MIDDLE');
    print hidden(-name=>'mode', -value=>'login_frontpage', -override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print end_form;
}


sub checkLoginDetails{
    my $loginUser = param('login_username');
    my $loginPassword = param('login_password');

    find_user($loginUser);
    my $result = "false";
    if($username){
        my @password = getListFromHeading("password", $loginUser);
        if($password[0] =~ $loginPassword){
            $result = "true";
            $loggedInUser = $loginUser;
        }
        
    }
    # die "$result $loginUser $username p: $loginPassword rp:$password[0] @password";
    return $result;

}
sub print_searchButton{
    print start_form(-name => 'searchButton', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );
    print image_button(-name=>'search_button',
                        -src=>"$images_dir/searchLogo.png",
                        -align=>'MIDDLE');
    print hidden(-name=>'mode', -value=>'display_searchBar', -override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print end_form;
}

sub print_headerBar{
    print start_form(-name => 'headerBar', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );
    print image_button(-name=>'search_button',
                        -src=>"$images_dir/header.png",
                        -align=>'MIDDLE');
    print hidden(-name=>'mode', -value=>'', -override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print end_form;
    
}

sub sendConfirmationEmail{
    # first, create your message
     my $newEmail = param('new_email');
     my $newUser = param('new_username');
     my $message = "Confirm your UNSW Pokedex account by clicking here:  
                             $cgi_url?mode=confirm_registration&confirmation_username=$newUser
                             \n";
    my $mailer = Mail::Mailer->new();
    #open mailer.
    $mailer->open({ From    => "lukasmarshall\@gmail.com",
                    To      => $newEmail,
                    Subject => "UNSW Pokedex Confirmation",
                  })
        or die "Can't open: $!\n";
    #print the message to mailer.
    print $mailer $message;
    $mailer->close();
  
}

sub confirmRegistraionFromEmail{
    changeUserInfo("confirmed:", "    true", param('confirmation_username'));
    $mode = 'new_user_confirmed';
    display_searchPage();
}

sub display_userNotFoundPage{
    $mode = 'user_not_found';
    display_searchPage();
}

sub print_addFriendButton{
    print start_form(-name => 'addFriendButton', 
                                -method  => 'GET', 
                                -enctype => &CGI::URL_ENCODED,
                                -value     => 'Gallery',
                            );
    print image_button(-name=>'addFriend_button',
                        -src=>"$images_dir/friendRequest.png",
                        -align=>'MIDDLE');
    print hidden(-name=>'mode', -value=>'send_friendRequest', -override=>1);
    print hidden(-name=>'username', -value=>$username,-override=>1);
    print hidden(-name=>'loggedInUser', -value=>$loggedInUser, -override=>1);
    print end_form;
}

sub sendFriendRequestEmail{
     # first, create your message
     my @email = getListFromHeading('email',$username);

     my $message = "$loggedInUser has sent you a friend request! If you want to be friends with this person, click here:
                             $cgi_url?mode=confirm_friend_request&person_accepting=$username&person_requesting=$loggedInUser
                             \n";
    my $mailer = Mail::Mailer->new();
    $mailer->open({ From    => "lukasmarshall\@gmail.com",
                    To      => $email[0],
                    Subject => "UNSW Pokedex Confirmation",
                  })
        or die "Can't open: $!\n";
    print $mailer $message;
    $mailer->close();
}

sub addFriend($$){
    my $person = $_[0];
    my $newFriend = $_[1];

    my @mates = getListFromHeading('mates',$person);
    push @mates, $newFriend;

    my $matesLine = join "\n     ", @mates;

    changeUserInfo('mates:', $matesLine, $person);

}