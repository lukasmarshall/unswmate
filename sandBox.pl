#! usr/bin/perl 

# $users_dir = "./users";
#$images_dir = "./images";
my $username = "Mary_Carey";


my $item = "name";




@mates = getListFromHeading($item, $username);


sub getListFromHeading($heading, $username){
    my $heading = $_[0];
   
    my $user = $_[1];
    
    my $details_filename = "./users/$user/details.txt";
    my @usernames;

    open FILE, $details_filename or die $!;
    my @lines = <FILE>;
    $inMatesList = 0;
    for $line (@lines){
        #check if line contains a colon - if so, a heading line.
        if($line =~ /.*:.*/){
            $inMatesList = 0;
        }
        #if we are in the middle of the mates list, add the line to the list of usernames.
        if($inMatesList == 1){ 
            $line =~ s/^\s+//;
            push @usernames, $line;
        }
        
        if($line =~ /.*$heading:.*/){
            $inMatesList = 1;
        }

    }
    return @usernames;
}