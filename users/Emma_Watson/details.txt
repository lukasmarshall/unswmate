username:
	Emma_Watson
password:
	apollo
student_number:
	3572734
mates:
	Wayne_Gretzky
	Franz_Fischler
	Celine_Dion
	Michael_Jackson
	Rudolph_Giuliani
	James_McGreevey
	Jon_Gruden
	Jennifer_Lopez
	Kate_Hudson
	Toni_Braxton
	Gwyneth_Paltrow
	Nicole_Kidman
	Venus_Williams
	Maria_Shriver
	Martha_Stewart
	Queen_Rania
	Patti_Labelle
	Colin_Farrell
	Laila_Ali
	Lisa_Marie_Presley
	Hillary_Clinton
	Amer_al-Saadi
	Oprah_Winfrey
	John_Travolta
	Carly_Fiorina
	Steffi_Graf
	Grant_Hackett
	David_Beckham
	Roseanne_Barr
	Matt_Dillon
	Madonna
	Tom_Hanks
	Christina_Aguilera
	Albert_Costa
	Bill_Clinton
	Tom_Cruise
	Jennifer_Capriati
	Felipe_Perez_Roque
	Conchita_Martinez
	Jennifer_Aniston
	Heath_Ledger
	Ben_Affleck
	Sourav_Ganguly
	Daniel_Radcliffe
	Arnold_Schwarzenegger
	Rod_Stewart
	Donatella_Versace
	Ronaldo_Luis_Nazario_de_Lima
	Richard_Gere
name:
	Emma Watson
degree:
	Computer Science/Digital Media
email:
	E.Watson@student.unsw.edu.au
gender:
	female
courses:
	2012 S1 COMP9331
	2012 S1 COMP9814
	2012 S1 GSOE9840
	2012 S2 COMP4418
	2012 S2 COMP9222
	2012 S2 GSOE9210
