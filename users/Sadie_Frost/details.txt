password:
	tiger
name:
	Sadie Frost
email:
	S.Frost@student.unsw.edu.au
username:
	Sadie_Frost
degree:
	Computer Science
mates:
	George_Lopez
	Vicente_Fox
	Vanessa_Incontrada
	Roberto_Carlos
	Jennifer_Lopez
	Tony_Blair
	Ilan_Ramon
	Kimi_Raikkonen
	Nicole_Kidman
	Steven_Spielberg
	Luis_Figo
	Vanessa_Redgrave
	Martina_McBride
	Mariangel_Ruiz_Torrealba
	Oprah_Winfrey
	James_Wolfensohn
	Elizabeth_Hurley
	John_Travolta
	Tiger_Woods
	Winona_Ryder
	Fabrice_Santoro
	David_Beckham
	Arianna_Huffington
	Bill_Clinton
	Tom_Cruise
	Jesse_Harris
	Bob_Hope
	Joan_Laporta
	Akhmed_Zakayev
	Heidi_Klum
	Paul_Sarbanes
	Carmen_Electra
	Meryl_Streep
gender:
	female
student_number:
	3584760
courses:
	2012 S1 ARTS1570
	2012 S1 ARTS1810
	2012 S1 ARTS1840
	2012 S1 ARTS3630
	2012 S2 ARTS1571
	2012 S2 ARTS1811
	2012 S2 COMP1917
	2012 S2 MATH1131
