username:
	Mary-Kate_Olsen
gender:
	female
student_number:
	3517112
name:
	Mary-Kate Olsen
mates:
	Ralf_Schumacher
	Kevin_Costner
	Jennifer_Lopez
	Nicanor_Duarte_Frutos
	Oscar_De_La_Hoya
	Azra_Akin
	Serena_Williams
	Guillermo_Coria
	Colin_Farrell
	Alejandro_Toledo
	Juan_Manuel_Marquez
	Scott_Peterson
	Kamal_Kharrazi
	Naomi_Watts
	Madonna
	Dale_Earnhardt_Jr
	Christina_Aguilera
	Luis_Gonzalez_Macchi
	Pete_Sampras
	Amelie_Mauresmo
	Anna_Kournikova
	Tom_Daschle
	Akhmed_Zakayev
	Heath_Ledger
	Doris_Roberts
	Andre_Agassi
	Ozzy_Osbourne
	Marco_Antonio_Barrera
	Holly_Hunter
	Jacques_Chirac
	Richard_Gere
	Evan_Rachel_Wood
degree:
	Computer Science
courses:
	2011 S2 COMP1917
	2011 S2 MATH1081
	2011 S2 MATH1131
	2011 S2 SENG1031
	2012 X1 COMP1927
	2012 X1 MATH1231
	2012 S1 COMP2041
	2012 S1 COMP2121
	2012 S1 MATH2011
	2012 S1 MATH2801
	2012 S2 COMP2911
	2012 S2 COMP3331
	2012 S2 MATH2120
	2012 S2 MATH2520
	2012 S2 MATH2601
email:
	Mary-K.Olsen@student.unsw.edu.au
password:
	7777777
