name:
	Bashar Assad
password:
	heather
student_number:
	3576438
degree:
	Information Technology
mates:
	Tom_Ridge
	Michael_Jackson
	Sally_Kirkland
	Sheryl_Crow
	Jodie_Foster
	Jean-Pierre_Raffarin
	Tony_Blair
	Queen_Latifah
	Alec_Baldwin
	Kimi_Raikkonen
	Katie_Harman
	Claire_Danes
	Serena_Williams
	Nicole_Kidman
	Julianne_Moore
	Steven_Spielberg
	Zoran_Djindjic
	Gus_Van_Sant
	Vladimir_Putin
	Winona_Ryder
	Paradorn_Srichaphan
	Madonna
	Nancy_Pelosi
	Salma_Hayek
	Luis_Gonzalez_Macchi
	Amelie_Mauresmo
	Sylvester_Stallone
	Heath_Ledger
	Abid_Hamid_Mahmud_Al-Tikriti
	Zico
	Daisy_Fuentes
	Angelina_Jolie
	Meryl_Streep
	Michelle_Yeoh
	Jacques_Chirac
	Victoria_Beckham
	Mohammad_Khatami
	Michael_Schumacher
gender:
	male
courses:
	2009 S1 ACCT1501
	2009 S1 COMP1917
	2009 S1 MATH1081
	2009 S1 MATH1131
	2009 S2 ACCT1511
	2009 S2 COMP1927
	2009 S2 MATH1231
	2009 S2 SENG1031
	2010 S1 COMP2111
	2010 S1 ECON1101
	2010 S1 INFS1603
	2010 S1 INFS2603
	2010 S1 SENG2010
	2010 S2 COMP2121
	2010 S2 COMP2911
	2010 S2 COMP3711
	2010 S2 FINS1613
	2010 S2 SENG2020
	2011 S1 COMP3141
	2011 S1 COMP3331
	2011 S1 FINS2624
	2011 S1 FINS3616
	2011 S1 SENG3010
	2011 S2 COMP3171
	2011 S2 COMP9444
	2011 S2 COMP9517
	2011 S2 FINS3641
	2011 S2 SENG3020
	2012 S1 COMP3311
	2012 S1 COMP9321
	2012 S1 SENG4921
	2012 S2 COMP9322
	2012 S2 COMP9323
	2012 S2 MATH2859
email:
	B.Assad@student.unsw.edu.au
username:
	Bashar_Assad
