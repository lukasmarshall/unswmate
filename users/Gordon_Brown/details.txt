courses:
	2009 S1 COMP1917
	2009 S1 INFS1603
	2009 S1 MATH1081
	2009 S1 MATH1141
	2009 S2 ACCT1501
	2009 S2 COMP1927
	2009 S2 MATH1251
	2009 S2 SENG1031
	2010 S1 ACCT1511
	2010 S1 COMP2111
	2010 S1 COMP3711
	2010 S1 ECON1101
	2010 S1 SENG2010
	2010 S2 COMP2121
	2010 S2 COMP2911
	2010 S2 ECON1102
	2010 S2 INFS2603
	2010 S2 SENG2020
	2011 S1 COMP3141
	2011 S1 COMP3331
	2011 S1 COMP3411
	2011 S1 SENG3010
	2011 S2 COMP3161
	2011 S2 COMP3421
	2011 S2 MATH2859
	2011 S2 SENG3020
	2012 S1 COMP3121
	2012 S1 FINS1612
	2012 S1 FINS1613
	2012 S1 SENG4921
	2012 S2 COMP3171
	2012 S2 FINS2624
	2012 S2 FINS3616
	2012 S2 MATH2859
name:
	Gordon Brown
degree:
	Computer Science and Eng
email:
	G.Brown@student.unsw.edu.au
password:
	andrea
mates:
	Wayne_Gretzky
	Vanessa_Incontrada
	Britney_Spears
	Fidel_Castro
	Debbie_Reynolds
	Pierce_Brosnan
	Liam_Neeson
	Jim_OBrien
	Claire_Danes
	Serena_Williams
	Venus_Williams
	Queen_Rania
	Steven_Spielberg
	Ian_Thorpe
	Vanessa_Redgrave
	Ahmed_Chalabi
	George_W_Bush
	Oprah_Winfrey
	Vladimir_Putin
	Gerhard_Schroeder
	Kamal_Kharrazi
	Robert_Redford
	Sarah_Jessica_Parker
	Pete_Sampras
	Scott_McClellan
	King_Abdullah_II
	Sepp_Blatter
	Jennifer_Aniston
	Mike_Weir
	Heather_Mills
	Zico
	Rob_Lowe
	Pete_Carroll
	Hugo_Chavez
	Jacques_Chirac
	Mohammad_Khatami
	Mikhail_Kasyanov
	Jennifer_Connelly
	JK_Rowling
	Sheryl_Crow
	Cindy_Crawford
	Sarah_Michelle_Gellar
	Tony_Blair
	Carlos_Moya
	Alastair_Campbell
	Gwyneth_Paltrow
	Alejandro_Toledo
	Robert_Kocharian
	Luis_Figo
	Barbara_Walters
	Alvaro_Uribe
	Hillary_Clinton
	Carly_Fiorina
	Demi_Moore
	Michael_Bloomberg
	Bill_Clinton
	Jonathan_Edwards
	Megan_Mullally
	Paul_Wolfowitz
	Derek_Jeter
	Angelina_Jolie
	Bertie_Ahern
	Reese_Witherspoon
	Daniel_Radcliffe
	Sharon_Stone
	Jose_Maria_Aznar
	Kate_Winslet
	Ethan_Hawke
	Michael_Schumacher
gender:
	male
username:
	Gordon_Brown
student_number:
	3560331
