email:
	B.Frist@student.unsw.edu.au
mates:
	Wayne_Gretzky
	Vicente_Fox
	Michael_Jackson
	Tony_Bennett
	Amelia_Vega
	Condoleezza_Rice
	Kevin_Costner
	Monica_Lewinsky
	Tony_Blair
	Oscar_De_La_Hoya
	Martin_Verkerk
	John_Swofford
	Barbra_Streisand
	Zoran_Djindjic
	Cecilia_Bolocco
	Harrison_Ford
	Alvaro_Uribe
	Hillary_Clinton
	George_W_Bush
	Scott_Peterson
	Martina_McBride
	Oprah_Winfrey
	Russell_Simmons
	Pierre_Pettigrew
	James_Wolfensohn
	Vladimir_Putin
	Rick_Dinse
	Abdullah_Gul
	Noelle_Bush
	Robert_Zoellick
	Mark_Philippoussis
	Arianna_Huffington
	Lars_Von_Trier
	Joseph_Biden
	Howard_Dean
	Bill_Clinton
	Jonathan_Edwards
	Tom_Daschle
	Bob_Hope
	Paul_Wolfowitz
	Heather_Mills
	Caroline_Kennedy
	Calista_Flockhart
	Hugo_Chavez
	Heidi_Fleiss
	Matthew_Perry
	Gloria_Macapagal_Arroyo
	Michael_Schumacher
	Tommy_Franks
courses:
	2007 S1 ACCT1501
	2007 S1 ECON1101
	2007 S1 MATH1081
	2007 S1 MATH1151
	2007 S2 ACCT1511
	2007 S2 ACTL1001
	2007 S2 COMP1911
	2007 S2 MATH1251
	2008 S1 ACTL2001
	2008 S1 ECON1102
	2008 S1 MATH2111
	2008 S1 MATH2901
	2008 S2 ACTL2003
	2008 S2 FINS1613
	2008 S2 MATH2501
	2008 S2 MATH2931
	2009 S1 ACTL2001
	2009 S1 ACTL3002
	2009 S1 MATH2011
	2009 S1 MATH3801
	2009 S2 ACTL3003
	2009 S2 ACTL3004
	2009 S2 MATH2120
	2009 S2 MATH2520
	2009 S2 MATH3821
	2010 S1 ACTL3001
	2010 S1 ACTL3002
	2010 S1 MATH3831
	2010 S1 MATH3911
	2010 S2 FINS2624
	2010 S2 MATH3121
	2010 S2 MATH3311
	2010 S2 MATH3821
	2011 S1 FINS3636
	2011 S1 MATH3161
	2011 S1 MATH3911
	2011 S2 FINS2643
	2011 S2 MATH2871
	2011 S2 MATH3121
	2011 S2 MATH3261
	2011 S2 MGMT1001
	2012 S1 MATH3811
	2012 S2 COMP9020
	2012 S2 COMP9024
	2012 S2 COMP9032
	2012 S2 COMP9311
password:
	heather
degree:
	Sci, Eng & Tech
name:
	Bill Frist
username:
	Bill_Frist
gender:
	male
student_number:
	3567872
