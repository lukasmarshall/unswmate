username:
	Ben_Curtis
degree:
	Electrical Engineering
name:
	Ben Curtis
student_number:
	3567515
courses:
	2012 S2 COMP1917
	2012 S2 COMP2121
	2012 S2 COMP3222
	2012 S2 COMP3511
gender:
	male
mates:
	George_Lopez
	Franz_Fischler
	Amelia_Vega
	Kate_Hudson
	JK_Rowling
	Jodie_Foster
	Ilan_Ramon
	Pierce_Brosnan
	Liam_Neeson
	Vitali_Klitschko
	Halle_Berry
	Gwyneth_Paltrow
	Venus_Williams
	Angela_Bassett
	Carrie-Anne_Moss
	Steven_Spielberg
	Justin_Timberlake
	Brad_Johnson
	Hillary_Clinton
	Mariangel_Ruiz_Torrealba
	Fabrice_Santoro
	Salma_Hayek
	Kristanna_Loken
	Pete_Sampras
	King_Abdullah_II
	Juan_Ignacio_Chela
	Justine_Pasek
	Oxana_Fedorova
	Richard_Armitage
	Juan_Carlos_Ferrero
	Emanuel_Ginobili
	Andre_Agassi
	Ozzy_Osbourne
	Daniel_Radcliffe
	Paul_Burrell
	Sarah_Hughes
	Faye_Dunaway
email:
	B.Curtis@student.unsw.edu.au
password:
	camaro
