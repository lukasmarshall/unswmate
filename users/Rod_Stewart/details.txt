username:
	Rod_Stewart
name:
	Rod Stewart
gender:
	male
courses:
	2011 S1 COMP1917
	2011 S1 ENGG1000
	2011 S1 MATH1141
	2011 S1 PHYS1131
	2011 S2 COMP1927
	2011 S2 ELEC1111
	2011 S2 MATH1241
	2011 S2 MATS1101
	2012 S1 COMP2911
	2012 S1 MATH2019
	2012 S1 MMAN1300
	2012 S1 MMAN2700
	2012 S2 COMP2041
	2012 S2 COMP2121
	2012 S2 COMP3431
	2012 S2 COMP3901
password:
	crystal
student_number:
	3527146
mates:
	Ludivine_Sagnier
	George_Lopez
	Julie_Gerberding
	Rudolph_Giuliani
	Michael_Jackson
	James_McGreevey
	Tony_Bennett
	Britney_Spears
	Emma_Watson
	Gianna_Angelopoulos-Daskalaki
	Terry_McAuliffe
	JK_Rowling
	Condoleezza_Rice
	Toni_Braxton
	Roger_Federer
	Alastair_Campbell
	Gwyneth_Paltrow
	Venus_Williams
	Maria_Shriver
	Martha_Stewart
	Queen_Rania
	Colin_Farrell
	Mark_Richt
	Patti_Labelle
	Steven_Spielberg
	Zoran_Djindjic
	Lisa_Marie_Presley
	George_W_Bush
	Hillary_Clinton
	Spencer_Abraham
	Oprah_Winfrey
	Uma_Thurman
	Mariah_Carey
	Mahmoud_Abbas
	John_Travolta
	Carly_Fiorina
	Natasha_McElhone
	Tiger_Woods
	Fabrice_Santoro
	David_Beckham
	Roseanne_Barr
	Rachel_Hunter
	Salma_Hayek
	Tom_Hanks
	Tim_Henman
	Howard_Dean
	Christina_Aguilera
	Hilary_Duff
	Albert_Costa
	Bill_Clinton
	Tom_Cruise
	Fernando_Gonzalez
	Bob_Hope
	Heath_Ledger
	Derek_Jeter
	Richard_Armitage
	Rita_Wilson
	Sandra_Bullock
	Sergio_Vieira_De_Mello
	Rob_Lowe
	Noah_Wyle
	Ronaldo_Luis_Nazario_de_Lima
degree:
	Computer Science
email:
	R.Stewart@student.unsw.edu.au
