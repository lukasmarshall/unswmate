email:
	R.Giuliani@student.unsw.edu.au
student_number:
	3523011
password:
	victoria
mates:
	Tom_Ridge
	Emma_Watson
	Liam_Neeson
	Jim_OBrien
	Serena_Williams
	Halle_Berry
	Venus_Williams
	Martha_Stewart
	Goldie_Hawn
	Queen_Rania
	Patti_Labelle
	Tommy_Thompson
	Steven_Spielberg
	Harrison_Ford
	Lisa_Marie_Presley
	Nestor_Kirchner
	Gus_Van_Sant
	Oprah_Winfrey
	Uma_Thurman
	Rick_Dinse
	Jennifer_Garner
	Mariah_Carey
	John_Travolta
	Tiger_Woods
	Winona_Ryder
	Robert_Zoellick
	Roseanne_Barr
	Bridgette_Wilson-Sampras
	Frank_Solich
	Rebecca_Romijn-Stamos
	Tom_Cruise
	Elizabeth_Dole
	Bob_Hope
	Jennifer_Aniston
	Leonardo_DiCaprio
	John_Kerry
	Cate_Blanchett
	Mathias_Reichhold
	Calista_Flockhart
	Rob_Lowe
	Pete_Carroll
	Hugo_Chavez
	Matthew_Perry
	Sharon_Osbourne
	Celine_Dion
	Dick_Clark
	Condoleezza_Rice
	JK_Rowling
	Jennifer_Lopez
	Tony_Blair
	Gwyneth_Paltrow
	Nicole_Kidman
	Maria_Shriver
	Hillary_Clinton
	James_Wolfensohn
	Elizabeth_Hurley
	Juan_Pablo_Montoya
	George_Clooney
	Grant_Hackett
	Adam_Sandler
	Salma_Hayek
	Michael_Bloomberg
	Christina_Aguilera
	Anna_Kournikova
	Bill_Clinton
	Jennifer_Capriati
	Paul_Wolfowitz
	Zinedine_Zidane
	Caroline_Kennedy
	Angelina_Jolie
	Mary_Steenburgen
	Bijan_Darvish
	Rod_Stewart
	Evan_Rachel_Wood
	Richard_Gere
name:
	Rudolph Giuliani
courses:
	2012 S1 COMP1917
	2012 S1 ENGG1000
	2012 S1 MATH1131
	2012 S1 PHYS1131
	2012 S2 COMP1927
	2012 S2 ELEC1111
	2012 S2 MATH1231
	2012 S2 PHYS1231
gender:
	male
degree:
	Science/Computer Science
username:
	Rudolph_Giuliani
