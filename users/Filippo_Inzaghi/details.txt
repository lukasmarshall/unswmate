gender:
	male
mates:
	Matt_Doherty
	Gloria_Trevi
	Roberto_Carlos
	Silvan_Shalom
	Jennifer_Lopez
	Monica_Lewinsky
	Carlo_Ancelotti
	Liam_Neeson
	Halle_Berry
	Mikhail_Youzhny
	Justin_Timberlake
	George_W_Bush
	Rick_Dinse
	Joschka_Fischer
	Junichiro_Koizumi
	Lucy_Liu
	David_Beckham
	Robert_Zoellick
	Tom_Cruise
	Rick_Perry
	King_Abdullah_II
	Jennifer_Capriati
	Joan_Laporta
	Sylvester_Stallone
	Jennifer_Aniston
	Zinedine_Zidane
	Leonardo_DiCaprio
	Cameron_Diaz
	Heather_Mills
	Ben_Affleck
	Sandra_Bullock
	Sergio_Vieira_De_Mello
	Sharon_Stone
	Arnold_Schwarzenegger
	Kate_Winslet
	Ethan_Hawke
	Gloria_Macapagal_Arroyo
email:
	F.Inzaghi@student.unsw.edu.au
courses:
	2010 S1 COMP1917
	2010 S1 ENGG1000
	2010 S1 MATH1151
	2010 S1 PHYS1131
	2010 S2 COMP1927
	2010 S2 ELEC1111
	2010 S2 MATH1251
	2010 S2 PHYS1231
	2011 S1 ACCT1501
	2011 S1 COMP2121
	2011 S1 ELEC2134
	2011 S1 MATH2069
	2011 S2 ACCT1511
	2011 S2 ACTL1001
	2011 S2 COMP2911
	2011 S2 ECON1101
	2012 S1 ACTL2001
	2012 S1 ECON1102
	2012 S1 FINS1613
	2012 S1 MGMT1001
	2012 S2 COMP3222
	2012 S2 ELEC2133
	2012 S2 FINS2624
	2012 S2 FINS3616
	2012 S2 MATH2099
password:
	0
degree:
	Computing
username:
	Filippo_Inzaghi
student_number:
	3553704
name:
	Filippo Inzaghi
