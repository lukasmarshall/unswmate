name:
	Sarah Jessica Parker
gender:
	female
email:
	S.J.Parker@student.unsw.edu.au
student_number:
	3534829
mates:
	Vicente_Fox
	Jennifer_Connelly
	Celine_Dion
	Leszek_Miller
	Andy_Roddick
	Sheryl_Crow
	Jodie_Foster
	Cindy_Crawford
	Carlos_Moya
	Liam_Neeson
	Claire_Danes
	Serena_Williams
	Gwyneth_Paltrow
	Venus_Williams
	Nicole_Kidman
	Goldie_Hawn
	Queen_Rania
	Julianne_Moore
	Colin_Farrell
	Robert_Bonner
	Michael_Powell
	Lisa_Marie_Presley
	Nestor_Kirchner
	Vladimir_Putin
	Richard_Myers
	Junichiro_Koizumi
	Winona_Ryder
	Lucy_Liu
	Martha_Lucia_Ramirez
	George_Clooney
	Adam_Sandler
	Demi_Moore
	Kristanna_Loken
	Anna_Kournikova
	Jonathan_Edwards
	Tom_Cruise
	Catherine_Zeta-Jones
	Jennifer_Capriati
	Megan_Mullally
	Amanda_Bynes
	Jennifer_Aniston
	Leonardo_DiCaprio
	Heather_Mills
	Cameron_Diaz
	Mike_Weir
	Stanley_McChrystal
	Ben_Affleck
	Angelina_Jolie
	Michelle_Kwan
	Gordon_Brown
	Bridget_Fonda
	Calista_Flockhart
	Rob_Lowe
	Meryl_Streep
	Holly_Hunter
	Arnold_Schwarzenegger
	Matthew_Perry
	Michael_Schumacher
	Richard_Gere
degree:
	Information Technology
password:
	sammy
username:
	Sarah_Jessica_Parker
courses:
	2011 S1 ACCT1501
	2011 S1 COMP1917
	2011 S1 INFS1603
	2011 S1 MATH1131
	2011 S2 COMP1927
	2011 S2 MATH1231
	2011 S2 MGMT1001
	2011 S2 SENG1031
	2012 S1 COMP2111
	2012 S1 COMP2911
	2012 S1 INFS2603
	2012 S1 MATH1081
	2012 S1 SENG2010
	2012 S2 COMP2121
	2012 S2 ECON1101
	2012 S2 FINS1613
	2012 S2 SENG2020
