email:
	M.Philippoussis@student.unsw.edu.au
name:
	Mark Philippoussis
gender:
	male
courses:
	2010 S1 COMP1917
	2010 S1 INFS1603
	2010 S1 MATH1081
	2010 S1 MATH1131
	2010 S2 CEIC1000
	2010 S2 COMP1927
	2010 S2 MATH1231
	2010 S2 SENG1031
	2011 S1 COMP2111
	2011 S1 COMP2121
	2011 S1 INFS2603
	2011 S1 PHYS1160
	2011 S1 SENG2010
	2011 S2 COMP2911
	2011 S2 COMP3711
	2011 S2 MATH2859
	2011 S2 SENG2020
	2012 X1 GENC6007
	2012 X1 GENS1004
	2012 S1 COMP2911
	2012 S1 COMP3141
	2012 S1 COMP3331
	2012 S1 SENG3010
	2012 S1 SENG3020
	2012 S2 COMP3171
	2012 S2 COMP3511
	2012 S2 COMP9321
	2012 S2 INFS3608
	2012 S2 MATH2859
degree:
	Computer Science
mates:
	Juanes
	Sally_Kirkland
	Britney_Spears
	Kevin_Costner
	Pamela_Anderson
	Augustin_Calleri
	Gary_Carter
	Azra_Akin
	Serena_Williams
	Martin_Verkerk
	Venus_Williams
	David_Nalbandian
	Mark_Richt
	Steven_Spielberg
	Michael_Winterbottom
	Ian_Thorpe
	Lisa_Marie_Presley
	Guillermo_Canas
	Gus_Van_Sant
	Pierre_Pettigrew
	Rick_Dinse
	Mariah_Carey
	Donald_Pettit
	Martha_Lucia_Ramirez
	Paradorn_Srichaphan
	Marcelo_Rios
	Steffi_Graf
	Roseanne_Barr
	Noelle_Bush
	Robert_Redford
	Bridgette_Wilson-Sampras
	Pete_Sampras
	Albert_Costa
	Howard_Dean
	King_Abdullah_II
	Charlton_Heston
	Zico
	Juan_Carlos_Ferrero
	Sourav_Ganguly
	Bill_Frist
	Ralf_Schumacher
	Roberto_Carlos
	Andy_Roddick
	Monica_Lewinsky
	Cindy_Crawford
	Jodie_Foster
	Roger_Federer
	Kurt_Warner
	Carlos_Moya
	Elizabeth_Smart
	Kimi_Raikkonen
	Gwyneth_Paltrow
	Nicole_Kidman
	Mikhail_Youzhny
	Guillermo_Coria
	Colin_Farrell
	Justin_Timberlake
	George_Papandreou
	Jayson_Williams
	Thierry_Falise
	Richard_Krajicek
	Mariangel_Ruiz_Torrealba
	Jiri_Novak
	Kelly_Clarkson
	Laura_Bush
	Junichiro_Koizumi
	Fabrice_Santoro
	Abdullah_Gul
	Grant_Hackett
	Lance_Armstrong
	Luis_Horna
	Demi_Moore
	Tim_Henman
	Christina_Aguilera
	Anna_Kournikova
	Bill_Clinton
	Rick_Perry
	Princess_Caroline
	Tom_Daschle
	Kjell_Magne_Bondevik
	Juan_Ignacio_Chela
	Fernando_Gonzalez
	Jacques_Rogge
	Cameron_Diaz
	Lleyton_Hewitt
	Maria_Soledad_Alvear_Valenzuela
	Wayne_Ferreira
	Andre_Agassi
	Michael_Chang
	Bijan_Darvish
	John_Abizaid
	Richard_Gere
student_number:
	3516414
username:
	Mark_Philippoussis
password:
	madison
