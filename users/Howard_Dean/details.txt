name:
	Howard Dean
student_number:
	3509941
mates:
	Kwon_Yang-sook
	Tom_Ridge
	Michael_Jackson
	Vicente_Fox
	Tony_Bennett
	Toni_Braxton
	Marcelo_Salas
	Alec_Baldwin
	Pierce_Brosnan
	Liam_Neeson
	Claire_Danes
	John_Swofford
	Tommy_Thompson
	Steven_Spielberg
	Ian_Thorpe
	George_W_Bush
	Oprah_Winfrey
	Vladimir_Putin
	Rick_Dinse
	Uma_Thurman
	John_Travolta
	Donald_Pettit
	Lucy_Liu
	Naomi_Watts
	Marcelo_Rios
	Matt_Dillon
	Noelle_Bush
	Nancy_Pelosi
	Roseanne_Barr
	Mark_Philippoussis
	Nia_Vardalos
	Bridgette_Wilson-Sampras
	Albert_Costa
	Tom_Cruise
	Gregg_Popovich
	Elizabeth_Dole
	Catherine_Zeta-Jones
	Bob_Hope
	Mike_Weir
	Heather_Mills
	Charlton_Heston
	Leonardo_DiCaprio
	Zico
	John_Kerry
	Richard_Armitage
	Juan_Carlos_Ferrero
	Ben_Affleck
	Cate_Blanchett
	Bill_Frist
	Renee_Zellweger
	Hugo_Chavez
	Matthew_Perry
	Mohammad_Khatami
	Terry_McAuliffe
	Gianna_Angelopoulos-Daskalaki
	JK_Rowling
	Kate_Hudson
	Jennifer_Lopez
	Monica_Lewinsky
	Jodie_Foster
	Ilan_Ramon
	Tony_Blair
	Kimi_Raikkonen
	Vitali_Klitschko
	Nicole_Kidman
	Maria_Shriver
	Colin_Farrell
	Robert_Kocharian
	Jayson_Williams
	George_Papandreou
	Brad_Johnson
	Hillary_Clinton
	Mariangel_Ruiz_Torrealba
	Martina_McBride
	James_Wolfensohn
	George_Clooney
	Arianna_Huffington
	Bill_Clinton
	Tom_Daschle
	Juan_Ignacio_Chela
	Conchita_Martinez
	Paul_Wolfowitz
	Cameron_Diaz
	Caroline_Kennedy
	Emanuel_Ginobili
	Bijan_Darvish
	Sharon_Stone
	Arnold_Schwarzenegger
	Rod_Stewart
	Noah_Wyle
	Victoria_Beckham
	Gloria_Macapagal_Arroyo
	Michael_Schumacher
	Tommy_Franks
email:
	H.Dean@student.unsw.edu.au
courses:
	2009 S2 COMP1917
	2009 S2 ECON1101
	2009 S2 ELEC1111
	2009 S2 MATH1131
	2010 S1 COMP1927
	2010 S1 GMAT4900
	2010 S1 MATH1081
	2010 S1 PSYC1001
	2010 S2 COMP2121
	2010 S2 COMP2911
	2010 S2 ENGG1000
	2010 S2 PSYC1011
	2011 X1 MATH1231
	2011 S1 COMP2121
	2011 S1 COMP2911
	2011 S1 COMP3331
	2011 S1 INFS1602
	2011 S2 COMP2041
	2011 S2 COMP2911
	2011 S2 INFS1602
	2011 S2 MATH1231
	2012 S1 COMP2041
	2012 S1 COMP2121
	2012 S1 COMP3331
	2012 S2 COMP3421
	2012 S2 COMP3511
	2012 S2 COMP4920
username:
	Howard_Dean
gender:
	male
password:
	john
degree:
	Mechanical & Manufacturing Eng
