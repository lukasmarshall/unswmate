email:
	C.Martinez@student.unsw.edu.au
gender:
	male
name:
	Conchita Martinez
student_number:
	3512279
username:
	Conchita_Martinez
degree:
	Arts
mates:
	Sharon_Osbourne
	Ralf_Schumacher
	George_Lopez
	Vicente_Fox
	Michael_Jackson
	Emma_Watson
	Tony_Bennett
	Jon_Gruden
	Terry_McAuliffe
	Andy_Roddick
	Monica_Lewinsky
	Roger_Federer
	Pierce_Brosnan
	Alec_Baldwin
	Julia_Tymoshenko
	Azra_Akin
	Serena_Williams
	Mikhail_Youzhny
	Venus_Williams
	John_Swofford
	Steven_Spielberg
	Jayson_Williams
	Zoran_Djindjic
	Katherine_Harris
	Harrison_Ford
	Laila_Ali
	Richard_Krajicek
	Brad_Johnson
	Nestor_Kirchner
	Hillary_Clinton
	George_W_Bush
	Martina_McBride
	Oprah_Winfrey
	James_Wolfensohn
	Rick_Dinse
	John_Travolta
	Lucy_Liu
	Paradorn_Srichaphan
	Steffi_Graf
	Robert_Zoellick
	Matt_Dillon
	Lance_Armstrong
	Tim_Henman
	Howard_Dean
	Pete_Sampras
	Amelie_Mauresmo
	Anna_Kournikova
	Bill_Clinton
	Princess_Caroline
	Elizabeth_Dole
	Jennifer_Capriati
	Paul_Wolfowitz
	Heather_Mills
	John_Kerry
	Vanessa_Williams
	Andre_Agassi
	Bill_Callahan
	Rob_Lowe
	Richard_Gere
	Tommy_Franks
password:
	rocket
courses:
	2012 S1 COMP1917
	2012 S1 ENGG1000
	2012 S1 MATH1131
	2012 S1 PHYS1121
	2012 S2 COMP1927
	2012 S2 ELEC1111
	2012 S2 MATH1231
	2012 S2 PHYS1221
