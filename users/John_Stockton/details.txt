degree:
	Computer Engineering
mates:
	George_Clooney
	Marcelo_Rios
	Jennifer_Connelly
	Ben_Howland
	Roseanne_Barr
	Carson_Palmer
	Jennifer_Lopez
	Pamela_Anderson
	Queen_Latifah
	Liza_Minnelli
	Sally_Field
	Julianne_Moore
	Barbra_Streisand
	Cecilia_Bolocco
	Brad_Johnson
	Pete_Carroll
	Meryl_Streep
	Oprah_Winfrey
	Uma_Thurman
	Richard_Gere
	John_Travolta
password:
	dolphins
gender:
	male
email:
	J.Stockton@student.unsw.edu.au
username:
	John_Stockton
student_number:
	3555714
courses:
	2011 S1 COMP1911
	2011 S1 ENGG1000
	2011 S1 MATH1131
	2011 S1 PHYS1131
	2011 S2 COMP1921
	2011 S2 ELEC1111
	2011 S2 MATH1231
	2011 S2 PHYS1231
	2012 X1 ELEC2134
	2012 X1 MINE1010
	2012 X1 PHYS1231
	2012 S1 ECON1101
	2012 S1 ELEC2133
	2012 S1 ELEC2141
	2012 S1 MATH2069
	2012 S2 COMP1921
	2012 S2 ELEC2142
	2012 S2 MATH2099
	2012 S2 PHYS1231
name:
	John Stockton
