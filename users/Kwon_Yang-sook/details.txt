gender:
	female
password:
	teens
name:
	Kwon Yang-sook
username:
	Kwon_Yang-sook
mates:
	Michael_Jackson
	Vicente_Fox
	Tony_Bennett
	Terry_McAuliffe
	Monica_Lewinsky
	Jodie_Foster
	Martha_Stewart
	Queen_Rania
	Benazir_Bhutto
	Steven_Spielberg
	Zoran_Djindjic
	Harrison_Ford
	Hillary_Clinton
	George_W_Bush
	Martina_McBride
	Oprah_Winfrey
	Mariah_Carey
	Lucy_Liu
	Martha_Lucia_Ramirez
	Noelle_Bush
	Robert_Zoellick
	Arianna_Huffington
	Howard_Dean
	Joseph_Biden
	Scott_McClellan
	Bill_Clinton
	Paul_Wolfowitz
	Heather_Mills
	Derek_Jeter
	Vanessa_Williams
	Caroline_Kennedy
	Bill_Callahan
	Hugo_Chavez
	Michael_Schumacher
	Mohammad_Khatami
student_number:
	3575441
courses:
	2012 S2 COMP1911
	2012 S2 ENGG1000
	2012 S2 MATH1131
	2012 S2 PHYS1121
email:
	K.Yang-sook@student.unsw.edu.au
degree:
	Mechanical & Manf Eng/Science
