email:
	A.Gul@student.unsw.edu.au
student_number:
	3523216
mates:
	Condoleezza_Rice
	Andy_Roddick
	Pamela_Anderson
	Marcelo_Salas
	Carlos_Moya
	Alec_Baldwin
	Julia_Tymoshenko
	Vitali_Klitschko
	Goldie_Hawn
	David_Nalbandian
	Cecilia_Bolocco
	Richard_Krajicek
	Ahmed_Chalabi
	Jiri_Novak
	Oprah_Winfrey
	Rick_Dinse
	Laura_Bush
	Mahmoud_Abbas
	Robert_Zoellick
	Mark_Philippoussis
	Tim_Henman
	Albert_Costa
	Pete_Sampras
	Scott_McClellan
	Bill_Clinton
	Jonathan_Edwards
	Tom_Daschle
	Amanda_Bynes
	Paul_Bremer
	Paul_Wolfowitz
	Zico
	Richard_Armitage
	Lleyton_Hewitt
	Juan_Carlos_Ferrero
	Sourav_Ganguly
	Andre_Agassi
	Bill_Frist
	Heidi_Fleiss
	Evan_Rachel_Wood
name:
	Abdullah Gul
courses:
	2011 S1 COMP9024
	2011 S1 COMP9041
	2011 S1 COMP9311
	2011 S1 COMP9331
	2011 S2 COMP9032
	2011 S2 COMP9441
	2011 S2 COMP9447
	2011 S2 COMP9517
	2012 X1 MGMT5712
	2012 S1 COMP9201
	2012 S1 COMP9318
	2012 S1 COMP9417
	2012 S1 GSOE9820
	2012 S2 COMP9332
	2012 S2 COMP9335
	2012 S2 GSOE9210
degree:
	Information Technology
gender:
	male
password:
	boston
username:
	Abdullah_Gul
