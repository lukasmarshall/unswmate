degree:
	Mechatronic Eng/Biomedical Eng
courses:
	2012 S2 COMP9321
	2012 S2 COMP9511
	2012 S2 GSOE9210
	2012 S2 GSOE9820
password:
	hardcore
username:
	Ray_Romano
mates:
	Silvan_Shalom
	Tony_Blair
	Roger_Federer
	Kurt_Warner
	Alec_Baldwin
	Claire_Danes
	Kimi_Raikkonen
	Maria_Shriver
	Julianne_Moore
	Mark_Richt
	Luis_Figo
	Debra_Messing
	Hillary_Clinton
	Vladimir_Putin
	Winona_Ryder
	Tiger_Woods
	Kamal_Kharrazi
	Adam_Sandler
	David_Beckham
	Lance_Armstrong
	Bridgette_Wilson-Sampras
	Antonio_Banderas
	Thomas_Rupprath
	Hilary_Duff
	Bill_Clinton
	Tom_Cruise
	Kjell_Magne_Bondevik
	Jesse_Harris
	Bob_Hope
	Doris_Roberts
	Mike_Weir
	Maria_Soledad_Alvear_Valenzuela
	Heidi_Klum
	Wayne_Ferreira
	Keanu_Reeves
	Arnold_Schwarzenegger
	Richard_Gere
	Mohammad_Khatami
gender:
	male
student_number:
	3583381
email:
	R.Romano@student.unsw.edu.au
name:
	Ray Romano
