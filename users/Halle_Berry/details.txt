name:
	Halle Berry
gender:
	female
mates:
	Filippo_Inzaghi
	Rudolph_Giuliani
	Michael_Jackson
	Kristin_Davis
	Jon_Gruden
	Tony_Bennett
	Britney_Spears
	Carlo_Ancelotti
	Jean-Pierre_Raffarin
	Queen_Latifah
	Pierce_Brosnan
	Alec_Baldwin
	Liam_Neeson
	Martha_Stewart
	Queen_Rania
	Sally_Field
	Tommy_Thompson
	Patti_Labelle
	Carrie-Anne_Moss
	Steven_Spielberg
	Robert_Bonner
	Katherine_Harris
	Harrison_Ford
	Lisa_Marie_Presley
	Nestor_Kirchner
	Amer_al-Saadi
	Oprah_Winfrey
	Pierre_Pettigrew
	Vladimir_Putin
	Uma_Thurman
	Jennifer_Garner
	Mariah_Carey
	Tiger_Woods
	Ben_Curtis
	David_Beckham
	Noelle_Bush
	Roseanne_Barr
	Nia_Vardalos
	Laura_Linney
	Tom_Hanks
	Rebecca_Romijn-Stamos
	Tom_Cruise
	Bob_Hope
	Jennifer_Aniston
	Liza_Minnelli
	Leonardo_DiCaprio
	Ben_Affleck
	Rita_Wilson
	Luiz_Inacio_Lula_da_Silva
	Rob_Lowe
	Hugo_Chavez
	Ronaldo_Luis_Nazario_de_Lima
	Jennifer_Connelly
	James_McGreevey
	Robbie_Williams
	Dick_Clark
	JK_Rowling
	Condoleezza_Rice
	Jennifer_Lopez
	Sheryl_Crow
	Cindy_Crawford
	Tony_Blair
	Gwyneth_Paltrow
	Maria_Shriver
	Angela_Bassett
	Colin_Farrell
	Justin_Timberlake
	Robert_Kocharian
	Michael_Powell
	Barbara_Walters
	Richard_Krajicek
	Hillary_Clinton
	Gian_Marco
	Carly_Fiorina
	Fabrice_Santoro
	George_Clooney
	Grant_Hackett
	Salma_Hayek
	Antonio_Banderas
	Tim_Henman
	Demi_Moore
	Arianna_Huffington
	Christina_Aguilera
	Bill_Clinton
	Sylvester_Stallone
	Ciro_Gomes
	Stanley_McChrystal
	Emanuel_Ginobili
	Bertie_Ahern
	Jose_Maria_Aznar
	Arnold_Schwarzenegger
	Sharon_Stone
	Gloria_Macapagal_Arroyo
email:
	H.Berry@student.unsw.edu.au
courses:
	2010 S1 COMP1917
	2010 S1 ENGG1000
	2010 S1 MATH1081
	2010 S1 MATH1141
	2010 S2 COMP1927
	2010 S2 COMP4121
	2010 S2 ECON1101
	2010 S2 MATH1241
	2011 S1 COMP2041
	2011 S1 COMP2911
	2011 S1 COMP3821
	2011 S1 MATH2901
	2011 S2 COMP2121
	2011 S2 COMP9844
	2011 S2 MATH2400
	2011 S2 MATH2601
	2011 S2 MATH2620
	2012 S1 COMP3411
	2012 S1 COMP6731
	2012 S1 ECON2112
	2012 S1 MATH3521
	2012 S2 COMP3171
	2012 S2 COMP4920
	2012 S2 COMP6714
	2012 S2 MATH5425
username:
	Halle_Berry
student_number:
	3597387
password:
	marlboro
degree:
	Computer Eng/Biomed Eng
