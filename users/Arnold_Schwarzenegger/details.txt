password:
	andrew
student_number:
	3562117
courses:
	2008 S1 COMP1911
	2008 S1 ENGG1000
	2008 S1 MATH1131
	2008 S1 PHYS1131
	2008 S2 BIOM1010
	2008 S2 MATH1231
	2008 S2 MMAN1130
	2008 S2 MMAN1300
	2009 S1 MATH2019
	2009 S1 MATS1101
	2009 S1 MMAN2400
	2009 S1 MMAN2700
	2009 S2 BIOM9420
	2009 S2 ELEC1111
	2009 S2 MATH2089
	2009 S2 MMAN2600
	2010 S1 MMAN2100
	2010 S1 MMAN3200
	2010 S1 MMAN3300
	2010 S1 PHSL2121
	2010 S2 BIOM9660
	2010 S2 MMAN3210
	2010 S2 MTRN3100
	2010 S2 MTRN3500
	2011 S1 MMAN3300
	2011 S1 MMAN3400
	2011 S1 MMAN4400
	2011 S1 MTRN3200
	2011 S2 ANAT2511
	2011 S2 BIOM9561
	2012 S1 BIOM5003
	2012 S1 MTRN4010
	2012 S1 MTRN4230
	2012 S2 BIOM9060
	2012 S2 BIOM9410
	2012 S2 BIOM9541
	2012 S2 COMP3511
email:
	A.Schwarzenegger@student.unsw.edu.au
gender:
	male
username:
	Arnold_Schwarzenegger
name:
	Arnold Schwarzenegger
degree:
	Computer Science
mates:
	Diana_Krall
	Tom_Ridge
	Filippo_Inzaghi
	Michael_Jackson
	Kristin_Davis
	Jon_Gruden
	Emma_Watson
	Britney_Spears
	Toni_Braxton
	Jean-Pierre_Raffarin
	Gary_Carter
	Alec_Baldwin
	Pierce_Brosnan
	Liam_Neeson
	Erik_Morales
	Sophia_Loren
	Halle_Berry
	Sally_Field
	Mike_Myers
	Julianne_Moore
	Tommy_Thompson
	Carrie-Anne_Moss
	Patti_Labelle
	Steven_Spielberg
	Robert_Bonner
	Debra_Messing
	Katherine_Harris
	Harrison_Ford
	Guillermo_Canas
	George_W_Bush
	Amer_al-Saadi
	Oprah_Winfrey
	Uma_Thurman
	Rick_Dinse
	Jennifer_Garner
	Mariah_Carey
	John_Travolta
	Tiger_Woods
	Richard_Virenque
	David_Beckham
	Nancy_Pelosi
	Noelle_Bush
	Nia_Vardalos
	Sarah_Jessica_Parker
	Tom_Hanks
	Pete_Sampras
	Hilary_Duff
	Howard_Dean
	Tom_Cruise
	Catherine_Zeta-Jones
	Akhmed_Zakayev
	Paul_Bremer
	Jennifer_Aniston
	Mike_Weir
	Leonardo_DiCaprio
	Charlton_Heston
	John_Kerry
	Paul_Sarbanes
	Ben_Affleck
	Keanu_Reeves
	Cate_Blanchett
	Sourav_Ganguly
	Mathias_Reichhold
	Calista_Flockhart
	Rob_Lowe
	Hugo_Chavez
	Michelle_Yeoh
	Jacques_Chirac
	Mohammad_Khatami
	Faye_Dunaway
	Jennifer_Connelly
	Hamid_Karzai
	Robbie_Williams
	Ray_Romano
	Gianna_Angelopoulos-Daskalaki
	Andy_Roddick
	Stockard_Channing
	Sheryl_Crow
	Cindy_Crawford
	Roger_Federer
	Tony_Blair
	Elizabeth_Smart
	Kimi_Raikkonen
	Maria_Shriver
	Angela_Bassett
	Barbra_Streisand
	Colin_Farrell
	George_Papandreou
	Barbara_Walters
	Richard_Krajicek
	Hillary_Clinton
	Thomas_OBrien
	Joschka_Fischer
	Laura_Bush
	George_Clooney
	Adam_Sandler
	Natalie_Cole
	Salma_Hayek
	Antonio_Banderas
	Tim_Henman
	Kristanna_Loken
	Demi_Moore
	Arianna_Huffington
	Christina_Aguilera
	Jonathan_Edwards
	Bill_Clinton
	Kjell_Magne_Bondevik
	Fernando_Gonzalez
	Megan_Mullally
	Ciro_Gomes
	Sylvester_Stallone
	Heath_Ledger
	Maria_Soledad_Alvear_Valenzuela
	Caroline_Kennedy
	Angelina_Jolie
	Sergio_Vieira_De_Mello
	Holly_Hunter
	Christine_Gregoire
	Meryl_Streep
	Bijan_Darvish
	Sharon_Stone
