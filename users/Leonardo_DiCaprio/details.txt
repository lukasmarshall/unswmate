courses:
	2010 S1 COMP1917
	2010 S1 INFS1603
	2010 S1 MATH1081
	2010 S1 MATH1141
	2010 S2 COMP1927
	2010 S2 ENGG1000
	2010 S2 MATH1231
	2010 S2 SENG1031
	2011 S1 COMP2111
	2011 S1 COMP2121
	2011 S1 GENS4010
	2011 S1 INFS2603
	2011 S1 SENG2010
	2011 S2 COMP2041
	2011 S2 COMP2911
	2011 S2 COMP3711
	2011 S2 MATH2859
	2011 S2 SENG2020
	2012 S1 COMP3141
	2012 S1 COMP3331
	2012 S1 COMP3821
	2012 S1 SENG3010
	2012 S1 SENG3020
	2012 S2 COMP3171
	2012 S2 COMP9321
	2012 S2 COMP9332
	2012 S2 PHYS1160
gender:
	male
degree:
	Computer Science
username:
	Leonardo_DiCaprio
name:
	Leonardo DiCaprio
mates:
	Rudolph_Giuliani
	Michael_Jackson
	Filippo_Inzaghi
	Vanessa_Incontrada
	Tony_Bennett
	Kevin_Costner
	Silvan_Shalom
	Emma_Thompson
	Fidel_Castro
	Toni_Braxton
	Nicanor_Duarte_Frutos
	Jean-Pierre_Raffarin
	Queen_Latifah
	Debbie_Reynolds
	Yasar_Yakis
	Pierce_Brosnan
	Alec_Baldwin
	Liam_Neeson
	Claire_Danes
	Halle_Berry
	Venus_Williams
	Goldie_Hawn
	John_Swofford
	Mike_Myers
	Julianne_Moore
	Tommy_Thompson
	Patti_Labelle
	Steven_Spielberg
	Michael_Winterbottom
	Harrison_Ford
	Ahmed_Chalabi
	George_W_Bush
	Oprah_Winfrey
	Vladimir_Putin
	Rick_Dinse
	Uma_Thurman
	Jennifer_Garner
	Mariah_Carey
	John_Travolta
	Winona_Ryder
	Naomi_Watts
	Kamal_Kharrazi
	David_Beckham
	Roseanne_Barr
	Madonna
	Nancy_Pelosi
	Robert_Redford
	Sarah_Jessica_Parker
	Nia_Vardalos
	Tom_Hanks
	Laura_Linney
	Hilary_Duff
	Howard_Dean
	Scott_McClellan
	Tom_Cruise
	Catherine_Zeta-Jones
	Akhmed_Zakayev
	Penelope_Cruz
	Amanda_Bynes
	Sepp_Blatter
	Jennifer_Aniston
	Norah_Jones
	Heather_Mills
	Heidi_Klum
	Ben_Affleck
	Sandra_Bullock
	Rita_Wilson
	Nathalie_Baye
	Keanu_Reeves
	Renee_Zellweger
	Rob_Lowe
	Mikhail_Kasyanov
	Mohammad_Khatami
	Sharon_Osbourne
	Ralf_Schumacher
	Franz_Fischler
	Jennifer_Connelly
	Leszek_Miller
	Roberto_Carlos
	Jennifer_Lopez
	JK_Rowling
	Kate_Hudson
	Monica_Lewinsky
	Jodie_Foster
	Cindy_Crawford
	Tony_Blair
	Ilan_Ramon
	Julia_Tymoshenko
	Maria_Shriver
	Nicole_Kidman
	Barbra_Streisand
	Colin_Farrell
	Justin_Timberlake
	Zoran_Djindjic
	George_Papandreou
	Robert_Kocharian
	Barbara_Walters
	Hillary_Clinton
	Martina_McBride
	Russell_Simmons
	Kelly_Clarkson
	Joschka_Fischer
	Laura_Bush
	Juan_Pablo_Montoya
	George_Clooney
	Catherine_Deneuve
	Antonio_Banderas
	Christina_Aguilera
	Bill_Clinton
	Tom_Daschle
	Kjell_Magne_Bondevik
	Sylvester_Stallone
	Heath_Ledger
	Cameron_Diaz
	Stanley_McChrystal
	Lleyton_Hewitt
	Ozzy_Osbourne
	Holly_Hunter
	Meryl_Streep
	Sharon_Stone
	Arnold_Schwarzenegger
	Ethan_Hawke
	Kate_Winslet
	Donatella_Versace
	Victoria_Beckham
	Richard_Gere
	Evan_Rachel_Wood
student_number:
	3593122
email:
	L.DiCaprio@student.unsw.edu.au
password:
	chelsea
