email:
	A.al-Saadi@student.unsw.edu.au
username:
	Amer_al-Saadi
mates:
	Wayne_Gretzky
	Kristin_Davis
	Madonna
	Emma_Watson
	Natalie_Cole
	Condoleezza_Rice
	Antonio_Banderas
	Halle_Berry
	Maria_Shriver
	Sylvester_Stallone
	Liza_Minnelli
	Luiz_Inacio_Lula_da_Silva
	Michelle_Kwan
	Hillary_Clinton
	Nestor_Kirchner
	Sharon_Stone
	Arnold_Schwarzenegger
	Daniel_Radcliffe
	Vladimir_Putin
	John_Travolta
	Sarah_Hughes
courses:
	2011 S1 CHEM1001
	2011 S1 COMP1911
	2011 S1 MATH1131
	2011 S1 PHYS1121
	2011 S2 ECON1101
	2011 S2 MATH1131
	2011 S2 MATS1101
	2011 S2 PHYS1131
	2012 X1 MATH1231
	2012 S1 MATH2011
	2012 S1 MATH2301
	2012 S1 MATH2801
	2012 S2 COMP1921
	2012 S2 MATH2120
	2012 S2 MATH2501
	2012 S2 MATH2520
	2012 S2 MATH2831
password:
	aaaaaa
gender:
	male
student_number:
	3544863
degree:
	Information Technology
name:
	Amer al-Saadi
