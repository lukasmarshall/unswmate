mates:
	Ludivine_Sagnier
	Arminio_Fraga
	Vicente_Fox
	Hamid_Karzai
	Tony_Bennett
	Terry_McAuliffe
	Amelia_Vega
	Christine_Baumgartner
	Condoleezza_Rice
	Tony_Blair
	Roger_Federer
	Carlos_Moya
	Alec_Baldwin
	Katie_Harman
	Julia_Tymoshenko
	John_Swofford
	Goldie_Hawn
	Carlos_Manuel_Pruneda
	Jan_Ullrich
	Jayson_Williams
	Robert_Kocharian
	Debra_Messing
	Thierry_Falise
	Katherine_Harris
	Tony_Shalhoub
	Alvaro_Uribe
	Hillary_Clinton
	Ahmed_Chalabi
	George_W_Bush
	Oprah_Winfrey
	Pierre_Pettigrew
	Vladimir_Putin
	Mahmoud_Abbas
	Laura_Bush
	Joschka_Fischer
	Aaron_Peirsol
	Tiger_Woods
	Abdullah_Gul
	David_Beckham
	Robert_Zoellick
	Laura_Linney
	Kristanna_Loken
	Scott_McClellan
	Bill_Clinton
	Amelie_Mauresmo
	Rick_Perry
	Jose_Canseco
	Megan_Mullally
	Bob_Hope
	Paul_Wolfowitz
	Ciro_Gomes
	Mike_Weir
	Maria_Soledad_Alvear_Valenzuela
	Richard_Armitage
	Angelina_Jolie
	Michelle_Kwan
	Intisar_Ajouri
	Sharon_Stone
	Arnold_Schwarzenegger
	John_Abizaid
	Tommy_Franks
username:
	Paul_Bremer
password:
	warrior
courses:
	2007 S1 COMP1911
	2007 S1 MATH1081
	2007 S1 MATH1131
	2007 S1 PHYS1121
	2007 S2 COMP1911
	2007 S2 INFS1603
	2007 S2 MATH1131
	2007 S2 SENG1031
	2008 S1 ACCT1501
	2008 S1 COMP1921
	2008 S1 MATH1081
	2008 S2 COMP2121
	2008 S2 COMP2911
	2009 S1 COMP2111
	2009 S1 COMP2911
	2009 S1 INFS2603
	2009 S1 SENG2010
	2009 S2 COMP3711
	2009 S2 MATH1231
	2009 S2 SENG2020
	2010 S1 COMP3311
	2010 S1 COMP3331
	2010 S1 ECON1101
	2010 S1 SENG3010
	2010 S2 COMP2121
	2010 S2 COMP9321
	2010 S2 INFS2607
	2011 X1 MATH1231
	2011 S1 BENV2425
	2011 S1 COMP3141
	2011 S1 COMP9322
	2011 S1 SENG4921
	2012 X1 MATH1231
	2012 S1 COMP2041
	2012 S1 GENE7801
	2012 S1 MGMT1001
	2012 S2 COMP3171
	2012 S2 MATH1231
name:
	Paul Bremer
gender:
	male
student_number:
	3523159
email:
	P.Bremer@student.unsw.edu.au
degree:
	Study Abroad Program
