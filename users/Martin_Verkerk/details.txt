gender:
	male
email:
	M.Verkerk@student.unsw.edu.au
courses:
	2009 S1 COMP1917
	2009 S1 ENGG1000
	2009 S1 INFS1603
	2009 S1 MATH1131
	2009 S2 COMP1927
	2009 S2 MARK1012
	2009 S2 MATH1081
	2009 S2 MATH1231
	2010 S1 ARTS1450
	2010 S1 COMP2911
	2010 S1 COMP3141
	2010 S1 COMP3331
	2010 S2 ARTS1451
	2010 S2 COMP2041
	2010 S2 COMP3151
	2010 S2 COMP3421
	2010 S2 MATH2400
	2011 S1 COMP2121
	2011 S1 COMP3121
	2011 S1 COMP3411
	2011 S1 GENS4010
	2011 S1 SENG4921
	2011 S2 COMP3161
	2011 S2 COMP3441
	2011 S2 COMP4418
	2011 S2 COMP9447
	2012 S1 COMP3311
	2012 S1 COMP3891
	2012 S1 COMP4141
	2012 S1 COMP6721
	2012 S2 COMP3171
	2012 S2 COMP4181
mates:
	Ludivine_Sagnier
	Sally_Kirkland
	Britney_Spears
	Kevin_Costner
	Silvan_Shalom
	Marcelo_Salas
	Queen_Latifah
	Augustin_Calleri
	Claire_Danes
	Serena_Williams
	Venus_Williams
	David_Nalbandian
	Steven_Spielberg
	Vanessa_Redgrave
	Guillermo_Canas
	Natalie_Maines
	Jennifer_Garner
	Paradorn_Srichaphan
	Marcelo_Rios
	Mark_Philippoussis
	Thomas_Rupprath
	Albert_Costa
	Lars_Von_Trier
	Charlton_Heston
	Juan_Carlos_Ferrero
	Bill_Frist
	Intisar_Ajouri
	Queen_Beatrix
	James_McGreevey
	Gianna_Angelopoulos-Daskalaki
	Terry_McAuliffe
	Andy_Roddick
	Jennifer_Lopez
	Monica_Lewinsky
	Stockard_Channing
	Jodie_Foster
	Sarah_Michelle_Gellar
	Roger_Federer
	Carlos_Moya
	Elizabeth_Smart
	Mikhail_Youzhny
	Nicole_Kidman
	Maria_Shriver
	Guillermo_Coria
	Colin_Farrell
	Justin_Timberlake
	Richard_Krajicek
	Jiri_Novak
	Fabrice_Santoro
	Luis_Horna
	Catherine_Deneuve
	Lance_Armstrong
	Tim_Henman
	Princess_Caroline
	Kjell_Magne_Bondevik
	Juan_Ignacio_Chela
	Fernando_Gonzalez
	Wayne_Ferreira
	Lleyton_Hewitt
	Andre_Agassi
	Michelle_Kwan
	Monica_Bellucci
	Holly_Hunter
	Meryl_Streep
	Noah_Wyle
	Richard_Gere
	Sarah_Hughes
student_number:
	3567809
username:
	Martin_Verkerk
name:
	Martin Verkerk
degree:
	Computer Eng/Biomed Eng
password:
	victoria
