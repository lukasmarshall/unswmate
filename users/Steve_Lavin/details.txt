courses:
	
degree:
	Computer Science
password:
	jaguar
username:
	Steve_Lavin
email:
	S.Lavin@student.unsw.edu.au
name:
	Steve Lavin
mates:
	Lucy_Liu
	Diana_Krall
	Arminio_Fraga
	Celine_Dion
	David_Beckham
	Ben_Howland
	Tony_Bennett
	Roseanne_Barr
	Catherine_Deneuve
	Nancy_Pelosi
	Robert_Redford
	Condoleezza_Rice
	Nia_Vardalos
	Antonio_Banderas
	Tony_Blair
	Mike_Myers
	Julianne_Moore
	Cameron_Diaz
	Barbra_Streisand
	Guillermo_Canas
	Brad_Johnson
	Jim_Tressel
	Ahmed_Chalabi
	Mark_Gottfried
	Doris_Schroeder
	Mariah_Carey
	Tommy_Franks
	Sarah_Hughes
gender:
	male
student_number:
	3586836
