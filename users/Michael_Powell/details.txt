email:
	M.Powell@student.unsw.edu.au
name:
	Michael Powell
username:
	Michael_Powell
gender:
	male
password:
	compaq
degree:
	Computer Science
courses:
	2009 S1 BABS1201
	2009 S1 CHEM1031
	2009 S1 COMP1917
	2009 S1 COMP3821
	2009 S1 MATH1141
	2009 S2 COMP1927
	2009 S2 COMP9844
	2009 S2 MATH1081
	2009 S2 MATH1241
	2010 S1 COMP2041
	2010 S1 COMP2121
	2010 S1 COMP2911
	2010 S2 COMP3161
	2010 S2 COMP4121
	2010 S2 COMP9000
	2010 S2 MATH1231
	2011 S1 COMP3411
	2011 S1 COMP3891
	2011 S1 COMP4141
	2011 S1 COMP9417
	2011 S2 COMP9447
	2012 S1 COMP3311
	2012 S1 COMP3901
	2012 S1 GENS4015
	2012 S2 COMP4910
	2012 S2 GENL2032
	2012 S2 MATH2400
	2012 S2 MATH3411
student_number:
	3571675
mates:
	Martha_Lucia_Ramirez
	George_Clooney
	Diana_Krall
	Dick_Clark
	Amelia_Vega
	Sarah_Jessica_Parker
	Tony_Blair
	Tom_Daschle
	Catherine_Zeta-Jones
	Megan_Mullally
	Halle_Berry
	Sylvester_Stallone
	Robert_Kocharian
	Ian_Thorpe
	Bertie_Ahern
	Christian_Longo
	Pierre_Pettigrew
	Paul_Burrell
	Richard_Gere
	Michael_Schumacher
