email:
	F.Castro@student.unsw.edu.au
name:
	Fidel Castro
student_number:
	3574263
mates:
	Franz_Fischler
	Michael_Jackson
	Hamid_Karzai
	Jon_Gruden
	Stockard_Channing
	Monica_Lewinsky
	Jean-Pierre_Raffarin
	Oscar_De_La_Hoya
	Julia_Tymoshenko
	Serena_Williams
	Maria_Shriver
	John_Swofford
	Robert_Bonner
	Robert_Kocharian
	Juan_Manuel_Marquez
	Barbara_Walters
	Christine_Todd_Whitman
	Natalie_Maines
	George_W_Bush
	Nestor_Kirchner
	Ahmed_Chalabi
	James_Wolfensohn
	Gerhard_Schroeder
	Vladimir_Putin
	Rick_Dinse
	Juan_Pablo_Montoya
	Kamal_Kharrazi
	Roseanne_Barr
	Robert_Redford
	Frank_Solich
	Michael_Bloomberg
	Anna_Kournikova
	Amelie_Mauresmo
	Kjell_Magne_Bondevik
	Felipe_Perez_Roque
	Akhmed_Zakayev
	Heather_Mills
	Leonardo_DiCaprio
	Zico
	Sourav_Ganguly
	Luiz_Inacio_Lula_da_Silva
	Gordon_Brown
	Pete_Carroll
	Marco_Antonio_Barrera
	Sharon_Stone
	Hugo_Chavez
	Daniel_Radcliffe
	Jacques_Chirac
	Mikhail_Kasyanov
	Mohammad_Khatami
username:
	Fidel_Castro
password:
	111111
courses:
	2012 S1 COMP2041
	2012 S1 COMP2121
	2012 S1 COMP2911
	2012 S1 ELEC1111
	2012 S2 COMP3331
	2012 S2 COMP3511
	2012 S2 MATH1231
	2012 S2 MATH3411
degree:
	Software Engineering
gender:
	male
